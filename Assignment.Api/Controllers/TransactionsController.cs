﻿using Assignment.Api.Extensions;
using Assignment.Api.Response;
using Assignment.Api.Services;
using Assignment.Backend.Model;
using Assignment.Backend.Repository;
using Assignment.Library.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TransactionsController : ControllerBase
    {
        private readonly ILogger<TransactionsController> _logger;
        private readonly ITransactionRepository _transactionRepository;
        private readonly IFileTranslatorService _fileTranslatorService;

        public TransactionsController(ILogger<TransactionsController> logger, ITransactionRepository transactionRepository, IFileTranslatorService fileTranslatorService)
        {
            _logger = logger;

            _transactionRepository = transactionRepository;

            _fileTranslatorService = fileTranslatorService;
        }

        [HttpPost, DisableRequestSizeLimit]
        [Route("upload")]
        public async Task<IActionResult> Upload()
        {
            var request = HttpContext.Request;

            try
            {
                if (request.Form.Files.Count == 0)
                {
                    _logger.LogDebug("Cannot find file uploaded");

                    return BadRequest();
                }

                var postedFile = request.Form.Files[0];

                var fileName = postedFile.FileName;

                _logger.LogDebug($"File name {fileName}");

                using var stream = postedFile.OpenReadStream();
                
                var transactions = _fileTranslatorService.Translate(fileName, stream);

                _transactionRepository.AddTransactions(transactions.Select(t => new Transaction
                {
                    TransactionId = t.TransactionId,
                    Amount = t.PaymentDetails.Amount,
                    CurrencyCode = t.PaymentDetails.CurrencyCode,
                    Status = Enum.GetName(typeof(TransactionStatus), t.Status),
                    TransactionDate = t.TransactionDate
                }));

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.StackTrace);

                var postedFile = request.Form.Files[0];

                using var stream = postedFile.OpenReadStream();

                using (StreamReader streamReader = new StreamReader(stream))
                {
                    var contents = streamReader.ReadToEnd();



                    _logger.LogError(contents);
                }

                return BadRequest();
            }
        }

        [HttpGet]
        [Route("currency/{currencyCode}")]
        public IEnumerable<TransactionResponse> GetTransactionResponsesByCurrencyCode([FromRoute] string currencyCode)
        {
            return _transactionRepository.GetTransactionsByCurrency(currencyCode).Select(t => t.ToTransactionResponse());
        }

        [HttpGet]
        [Route("date/{startDate}/{endDate}")]
        public IEnumerable<TransactionResponse> GetTransactionResponsesByDate([FromRoute] DateTime startDate, [FromRoute] DateTime endDate)
        {
            return _transactionRepository.GetTransactionsByDate(startDate, endDate).Select(t => t.ToTransactionResponse());
        }

        [HttpGet]
        [Route("status/{status}")]
        public IEnumerable<TransactionResponse> GetTransactionResponsesByStatus([FromRoute] string status)
        {
            return _transactionRepository.GetTransactionsByStatus(status).Select(t => t.ToTransactionResponse());
        }
    }
}
