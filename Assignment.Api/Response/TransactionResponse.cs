﻿namespace Assignment.Api.Response
{
    public class TransactionResponse
    {
        public string Id { get; set; }
        public string Payment { get; set; }
        public string Status { get; set; }
    }
}
