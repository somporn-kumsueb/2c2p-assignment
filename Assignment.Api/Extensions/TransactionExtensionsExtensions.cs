﻿using Assignment.Api.Response;
using Assignment.Backend.Model;
using Assignment.Library.Enums;
using System;

namespace Assignment.Api.Extensions
{
    public static class TransactionExtensions
    {
        public static TransactionResponse ToTransactionResponse(this Transaction transaction)
        {
            return new TransactionResponse
            {
                Id = transaction.TransactionId,
                Payment = $"{transaction.Amount} {transaction.CurrencyCode}",
                Status = transaction.Status.ToStatus().ToText()
            };
        }

        private static TransactionStatus ToStatus(this string statusText)
        {
            if (Enum.TryParse(statusText, out TransactionStatus statusEnum))
            {
                return statusEnum;
            }

            throw new InvalidCastException();
        }

        private static string ToText(this TransactionStatus status)
        {
            switch (status)
            {
                case TransactionStatus.Approved: return "A";
                case TransactionStatus.Failed:
                case TransactionStatus.Rejected: return "R";
                case TransactionStatus.Finished:
                case TransactionStatus.Done: return "D";
            }

            throw new InvalidCastException();
        }
    }
}
