﻿using Assignment.Library;
using Assignment.Library.Parsers;
using Assignment.Library.Translators;
using System;
using System.Collections.Generic;
using System.IO;

namespace Assignment.Api.Services
{
    public interface IFileTranslatorService
    {
        IEnumerable<Transaction> Translate(string fileName, Stream stream);
    }

    public class FileTranslatorService : IFileTranslatorService
    {
        private readonly ITransactionParser _transactionParser = new TransactionParser();
        private readonly ITransactionParser _xmlTransactionParser = new XmlTransactionParser();
        private readonly ITranslator _csvTranslator;
        private readonly ITranslator _xmlTranslator;

        public FileTranslatorService(ITranslator csvTranslator, ITranslator xmlTranslator)
        {
            _csvTranslator = csvTranslator;
            _xmlTranslator = xmlTranslator;
        }

        public FileTranslatorService()
        {
            _csvTranslator = new CsvTranslator(_transactionParser);
            _xmlTranslator = new XmlTranslator(_xmlTransactionParser);
        }

        public IEnumerable<Transaction> Translate(string fileName, Stream stream)
        {

            var extension = Path.GetExtension(fileName);

            return extension switch
            {
                ".csv" => _csvTranslator.Translator(stream),
                ".xml" => _xmlTranslator.Translator(stream),
                _ => throw new InvalidOperationException(),
            };
        }
    }
}
