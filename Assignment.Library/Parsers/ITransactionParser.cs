﻿using Assignment.Library.Enums;
using System;
using System.Globalization;

namespace Assignment.Library.Parsers
{
    public interface ITransactionParser
    {
        decimal AmountParser(string text);

        DateTime DateParser(string text);

        TransactionStatus StatusParser(string text);
    }

    public class TransactionParser : ITransactionParser
    {
        public decimal AmountParser(string text)
        {
            if (decimal.TryParse(text, out var amount))
            {
                return amount;
            }

            throw new InvalidCastException();
        }

        public virtual DateTime DateParser(string text)
        {
            if (DateTime.TryParseExact(text, "dd/MM/yyyy hh:mm:ss", new CultureInfo("en-US"), DateTimeStyles.None, out var date))
            {
                return date;
            }

            throw new InvalidCastException();
        }

        public TransactionStatus StatusParser(string text)
        {
            if (Enum.TryParse<TransactionStatus>(text, out var status))
            {
                return status;
            }

            throw new InvalidCastException();
        }
    }

    public class XmlTransactionParser : TransactionParser
    {
        public override DateTime DateParser(string text)
        {
            // 2019-01-23T13:45:10
            if (DateTime.TryParse(text, out var date))
            {
                return date;
            }

            throw new InvalidCastException();
        }
    }
}
