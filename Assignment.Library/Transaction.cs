﻿using Assignment.Library.Enums;
using System;

namespace Assignment.Library.Enums
{
    public enum TransactionStatus
    {
        Approved,
        Failed,
        Finished,
        Rejected,
        Done
    }
}

namespace Assignment.Library
{
    public class PaymentDetails
    {
        public decimal Amount { get; set; }

        public string CurrencyCode { get; set; }
    }

    public class Transaction
    {
        public string TransactionId { get; set; }

        public DateTime TransactionDate { get; set; }

        public TransactionStatus Status { get; set; }

        public PaymentDetails PaymentDetails { get; set; }
    }
}
