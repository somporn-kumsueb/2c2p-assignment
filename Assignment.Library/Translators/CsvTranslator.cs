﻿using Assignment.Library.Parsers;
using System.Collections.Generic;
using System.IO;

namespace Assignment.Library.Translators
{
    public class CsvTranslator : ITranslator
    {
        private readonly ITransactionParser _transactionParser;

        public CsvTranslator(ITransactionParser transactionParser)
        {
            this._transactionParser = transactionParser;
        }

        public IEnumerable<Transaction> Translator(Stream stream)
        {
            using (StreamReader streamReader = new StreamReader(stream))
            {
                while (!streamReader.EndOfStream)
                {
                    var values = streamReader.ReadLine().Replace("\"", "").Replace("\\", "").Split(',');

                    yield return new Transaction
                    {
                        TransactionId = values[0],
                        PaymentDetails = new PaymentDetails
                        {
                            Amount = this._transactionParser.AmountParser(values[1]),
                            CurrencyCode = values[2]
                        },
                        TransactionDate = this._transactionParser.DateParser(values[3]),
                        Status = this._transactionParser.StatusParser(values[4])
                    };
                }
            }
        }
    }
}
