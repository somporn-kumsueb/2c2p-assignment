﻿using Assignment.Library.Parsers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Assignment.Library.Translators
{
    public class XmlTranslator : ITranslator
    {
        private readonly ITransactionParser _transactionParser;

        public XmlTranslator(ITransactionParser transactionParser)
        {
            this._transactionParser = transactionParser;
        }

        public IEnumerable<Transaction> Translator(Stream stream)
        {
            try
            {
                using (StreamReader streamReader = new StreamReader(stream))
                {
                    var xml = streamReader.ReadToEnd().Replace("\n", "").Replace("\r", "").Replace("\t", "").Replace("\\", "");

                    var doc = XDocument.Parse(xml);

                    return doc.Descendants("Transaction").Select(t => new Transaction
                    {
                        TransactionId = (string)t.Attribute("id"),
                        Status = this._transactionParser.StatusParser((string)t.Element("Status")),
                        TransactionDate = this._transactionParser.DateParser((string)t.Element("TransactionDate")),
                        PaymentDetails = new PaymentDetails
                        {
                            Amount = this._transactionParser.AmountParser((string)t.Element("PaymentDetails").Element("Amount")),
                            CurrencyCode = (string)t.Element("PaymentDetails").Element("CurrencyCode")
                        }
                    });
                }

                throw new InvalidCastException();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
