﻿using System.Collections.Generic;
using System.IO;

namespace Assignment.Library.Translators
{
    public interface ITranslator
    {
        IEnumerable<Transaction> Translator(Stream stream);
    }
}
