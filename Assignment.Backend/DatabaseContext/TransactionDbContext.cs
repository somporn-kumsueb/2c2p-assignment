﻿using Assignment.Backend.Model;
using Microsoft.EntityFrameworkCore;

namespace Assignment.Backend.DatabaseContext
{
    public class TransactionDbContext : DbContext
    {
        public DbSet<Transaction> Transactions { get; set; }

        public TransactionDbContext(DbContextOptions<TransactionDbContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Transaction>().Property(t => t.Id).ValueGeneratedOnAdd();
        }
    }
}
