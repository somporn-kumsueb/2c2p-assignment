﻿using Assignment.Backend.DatabaseContext;
using Assignment.Backend.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Assignment.Backend.Repository
{
    public interface ITransactionRepository
    {
        bool AddTransaction(Transaction transaction);

        bool AddTransactions(IEnumerable<Transaction> transactions);

        IEnumerable<Transaction> GetTransactionsByCurrency(string currencyCode);

        IEnumerable<Transaction> GetTransactionsByDate(DateTime startDate, DateTime endDate);

        IEnumerable<Transaction> GetTransactionsByStatus(string status);
    }

    public class TransactionRepository : ITransactionRepository
    {

        private TransactionDbContext _dbContext;
        private ILogger<TransactionRepository> _logger;

        public TransactionRepository(ILogger<TransactionRepository> logger, TransactionDbContext dbContext)
        {
            this._logger = logger;

            this._dbContext = dbContext;
        }

        public bool AddTransaction(Transaction transaction)
        {
            try
            {
                this._dbContext.Transactions.Add(transaction);

                this._dbContext.SaveChanges();

                return true;
            }
            catch(Exception ex)
            {
                this._logger.LogError(ex.StackTrace);

                return false;
            }
        }

        public bool AddTransactions(IEnumerable<Transaction> transactions)
        {
            try
            {
                foreach (var transaction in transactions)
                {
                    this._dbContext.Transactions.Add(transaction);
                }

                this._dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                this._logger.LogError(ex.StackTrace);

                return false;
            }
        }

        public IEnumerable<Transaction> GetTransactionsByCurrency(string currencyCode)
        {
            return this._dbContext.Transactions.Where(t => t.CurrencyCode == currencyCode);
        }

        public IEnumerable<Transaction> GetTransactionsByDate(DateTime startDate, DateTime endDate)
        {
            return this._dbContext.Transactions.Where(t => t.TransactionDate.Date >= startDate.Date && t.TransactionDate.Date <= endDate.Date);
        }

        public IEnumerable<Transaction> GetTransactionsByStatus(string status)
        {
            return this._dbContext.Transactions.Where(t => string.Compare(t.Status, status, StringComparison.OrdinalIgnoreCase) == 0);
        }
    }
}
